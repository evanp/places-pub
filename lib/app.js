// app.js -- main entry point for places.pub server
//
//  Copyright 2017 Evan Prodromou <evan@prodromou.name>
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

const fs = require('fs')
const express = require('express')
const path = require('path')
const pkg = path.join(__dirname, '..', 'package.json')
const {name, version} = JSON.parse(fs.readFileSync(pkg, {encoding: 'utf8'}))
const serverTag = `${name}/${version}`

const {argv} = require('yargs')
  .usage('Usage: $0 [options]')
  .number('p')
  .alias('p', 'port')
  .describe('p', 'port to listen on')
  .default('p', 80)
  .alias('r', 'root')
  .describe('r', 'root for local urls')
  .default('r', 'https://places.pub')
  .alias('e', 'email')
  .describe('e', 'email address to identify requests')
  .default('e', 'unit.test@places.pub')
  .alias('o', 'overpass')
  .describe('o', 'endpoint for Overpass API')
  .default('o', 'https://overpass.places.pub/api/interpreter')
  .number('l')
  .alias('l', 'limit')
  .describe('l', 'limit of concurrent connections for Overpass API')
  .default('l', 2)
  .env('PLACES_PUB')
  .alias('c', 'config')
  .describe('c', 'Config file')
  .default('c', '/etc/places.pub.json')
  .config('config')
  .help('h')
  .alias('h', 'help')

const app = express()

app.argv = argv

app.disable('x-powered-by')

app.use((req, res, next) => {
  res.set('Server', serverTag)
  next()
})

app.set('views', path.join(__dirname, '..', 'views'))
app.set('view engine', 'pug')

app.use(express.static(path.join(__dirname, '..', 'public')))
app.use('/jquery', express.static(path.join(__dirname, '..', 'node_modules', 'jquery', 'dist')))
app.use('/bootstrap', express.static(path.join(__dirname, '..', 'node_modules', 'bootstrap', 'dist')))

app.use('/osm', require('./osm')(app.argv))

app.get('/', (req, res) => {
  res.render('index', {title: 'Home'})
})

app.get('/api', (req, res) => {
  res.render('api', {title: 'API'})
})

app.get('/version', (req, res) => {
  res.json(version)
})

app.use((err, req, res, next) => {
  if (req.is('json')) {
    res.json({
      status: 'Error',
      message: err.message
    })
  } else {
    next(err)
  }
})

app.listen(app.argv.port, () => {
  console.log(`Server listening on localhost:${app.argv.port}`)
})
