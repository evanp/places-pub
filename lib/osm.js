// osm.js -- OpenStreetMap endpoints for places.pub
//
//  Copyright 2017 Evan Prodromou <evan@prodromou.name>
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

const express = require('express')
const fetch = require('node-fetch')
const qs = require('querystring')
const _ = require('lodash')
const debug = require('debug')('places-pub:osm')
const GeoPoint = require('geopoint')
const {queue} = require('async')

const AS2 = 'https://www.w3.org/ns/activitystreams'
const VCARD = 'http://www.w3.org/2006/vcard/ns#'
const CC = 'http://creativecommons.org/ns#'
const ODBL = 'http://opendatacommons.org/licenses/odbl/'
const DEFAULT_DISTANCE = 50
const PAGE_LENGTH = 10

function makeOSM (argv) {
  function makeURI (path) {
    return `${argv.root}${path}`
  }

  function setAddressProperty (obj, key, value) {
    if (!obj['vcard:hasAddress']) {
      obj['vcard:hasAddress'] = {
        type: 'vcard:Address'
      }
    }
    obj['vcard:hasAddress'][`vcard:${key}`] = value
  }

  function nodeToAS2 (nplace) {
    const as2place = {
      'id': makeURI(`/osm/node/${nplace.id}`),
      type: 'Place',
      latitude: Number(nplace.lat),
      longitude: Number(nplace.lon)
    }
    if (nplace.tags) {
      if (nplace.tags.name) {
        as2place.name = nplace.tags.name
      }
      if (nplace.tags['addr:housenumber'] && nplace.tags['addr:street']) {
        setAddressProperty(as2place, 'street-address', `${nplace.tags['addr:housenumber']} ${nplace.tags['addr:street']}`)
      } else if (nplace.tags['addr:street']) {
        setAddressProperty(as2place, 'street-address', nplace.tags['addr:street'])
      }
      if (nplace.tags['addr:postcode']) {
        setAddressProperty(as2place, 'postal-code', nplace.tags['addr:postcode'])
      }
    }
    if (nplace.timestamp) {
      as2place.updated = nplace.timestamp
    }
    return as2place
  }

  function wayToAS2 (way) {
    const as2place = {
      'id': makeURI(`/osm/way/${way.id}`),
      type: 'Place'
    }
    if (way.center) {
      as2place.latitude = Number(way.center.lat)
      as2place.longitude = Number(way.center.lon)
    }
    if (way.tags) {
      if (way.tags.name) {
        as2place.name = way.tags.name
      }
      if (way.tags['addr:housenumber'] && way.tags['addr:street']) {
        setAddressProperty(as2place, 'street-address', `${way.tags['addr:housenumber']} ${way.tags['addr:street']}`)
      } else if (way.tags['addr:street']) {
        setAddressProperty(as2place, 'street-address', way.tags['addr:street'])
      }
      if (way.tags['addr:postcode']) {
        setAddressProperty(as2place, 'postal-code', way.tags['addr:postcode'])
      }
      if (way.tags['addr:city']) {
        setAddressProperty(as2place, 'locality', way.tags['addr:city'])
      }
    }
    if (way.timestamp) {
      as2place.updated = way.timestamp
    }
    return as2place
  }

  function featureToAS2 (feature) {
    if (feature.type === 'node') {
      return nodeToAS2(feature)
    } else if (feature.type === 'way') {
      return wayToAS2(feature)
    } else {
      throw new Error(`Unexpected feature type: ${feature.type}`)
    }
  }

  function viewbox (lat, lon, d) {
    if (!(lat && lon)) {
      return undefined
    }
    debug({lat: lat, lon: lon, d: d})
    const gp = new GeoPoint(Number(lat), Number(lon))
    const bb = gp.boundingCoordinates(d / 1000, null, true)
    debug(bb)
    return `${bb[0].latitude().toFixed(5)},${bb[0].longitude().toFixed(5)},${bb[1].latitude().toFixed(5)},${bb[1].longitude().toFixed(5)}`
  }

  async function overpassQuery (task) {
    const {app, query} = task
    const qa = {
      data: `[output:json];${query};out meta center;`
    }
    debug(qa)
    const qp = qs.stringify(qa)
    debug(qp)
    const url = `${app.argv.overpass}?${qp}`
    // TODO: cache results
    debug(`fetching ${url}`)
    const res = await fetch(url)
    debug(`Got status ${res.status} for ${url}`)
    if (res.status !== 200) {
      debug(`Got an error: ${res.status} fetching ${url}`)
      debug(`Headers: ${JSON.stringify(res.headers)} fetching ${url}`)
      const text = await res.text()
      debug(`Got error text: '${text}' fetching ${url}`)
      throw new Error(text)
    }
    debug(`Parsing JSON output from ${url}`)
    const njson = await res.json()
    debug(`Finished parsing JSON output from ${url}`)
    debug(JSON.stringify(njson, null, 2))
    return njson
  }

  const q = queue(overpassQuery, argv.limit)

  async function interpret (app, query) {
    return new Promise((resolve, reject) => {
      return q.push({app: app, query: query}, (err, njson) => {
        if (err) {
          reject(err)
        } else {
          resolve(njson)
        }
      })
    })
  }

  const osm = express.Router()

  function as2Document (req, res, obj) {
    // Top-level boilerplate for AS2 and OSM
    const base = {
      '@context': [AS2, {vcard: VCARD, cc: CC}],
      'cc:license': ODBL,
      'cc:attributionName': 'OpenStreetMap contributors',
      'cc:attributionURL': 'http://www.openstreetmap.org/copyright'
    }
    const doc = _.extend(base, obj)
    // For cache
    if (doc.updated) {
      res.set('Last-Modified', (new Date(doc.updated)).toUTCString())
    }
    // Content negotiation, mostly
    if (req.accepts('application/activity+json')) {
      res.type('application/activity+json')
      res.send(Buffer.from(JSON.stringify(doc), 'utf8'))
    } else if (req.accepts('application/ld+json')) {
      res.type('application/ld+json; profile="https://www.w3.org/ns/activitystreams"')
      res.send(Buffer.from(JSON.stringify(doc), 'utf8'))
    } else if (req.accepts('application/json')) {
      res.json(doc)
    } else {
      // :(
      res.sendStatus(406)
    }
  }

  function searchSummary (q, lat, lon, d) {
    let summary = 'search'
    if (q) {
      summary = `${summary} for '${q}'`
    }
    if (lat && lon) {
      summary = `${summary} within ${d} meters of [${lat},${lon}]`
    }
    return summary
  }

  // We force the parameters in our search IDs to be in a fixed order

  function searchID (query) {
    const params = {}
    for (const prop of ['q', 'lat', 'lon', 'd', 'page']) {
      if (query[prop]) {
        params[prop] = query[prop]
      }
    }
    // We don't want a distance if it's not a geographical query
    if (params.d && !(params.lat && params.lon)) {
      delete params.d
    }
    const qp = qs.stringify(params)
    return makeURI(`/osm/search?${qp}`)
  }

  function normalizeQuery (query) {
    const nq = _.pick(query, ['q', 'lat', 'lon', 'd', 'page'])
    if (nq.q) {
      nq.q.replace('+', ' ')
    }
    // We only accept 5 decimal places, ~1m resolution
    if (nq.lat) {
      nq.lat = Number(Number(nq.lat).toFixed(5))
    }
    if (nq.lon) {
      nq.lon = Number(Number(nq.lon).toFixed(5))
    }

    if (nq.d) {
      nq.d = Number(nq.d)
    } else if (nq.lat && nq.lon) {
      nq.d = DEFAULT_DISTANCE
    }

    if (nq.page) {
      nq.page = Number(nq.page)
    }
    return nq
  }

  osm.get('/search', async (req, res, next) => {
    debug('osm /search route matched')
    const {page} = req.query
    const nquery = normalizeQuery(req.query)
    const {q, lat, lon} = nquery
    const {d} = nquery
    if (!q && (!lat && !lon)) {
      return next(new Error("Must define either 'q' or 'lat' and 'lon'"))
    }
    let query = null
    let vb = null
    if (lat && lon) {
      vb = viewbox(lat, lon, d)
    }
    if (q && lat && lon) {
      query = `(node["name"~"^${q}"](${vb});way["name"~"^${q}"](${vb}))`
    } else if (q) {
      query = `(node["name"~"^${q}"];way["name"~"^${q}"])`
    } else if (lat && lon) {
      query = `(node["name"](${vb});way["name"](${vb}))`
    }
    try {
      const njson = await interpret(req.app, query)
      if (!_.isObject(njson) || !_.isArray(njson.elements)) {
        throw new Error(`Unexpected result type: ${typeof njson}`)
      }
      debug(`Converting overpass data to AS2 for ${req.query}`)
      const items = njson.elements.map(featureToAS2)
      const ePage = Number(page) || 1
      const [offset, limit] = [(ePage - 1) * PAGE_LENGTH, PAGE_LENGTH]
      if (page && (offset > items.length)) {
        throw new Error(`No such page ${page}`)
      }
      const pa = _.extend({}, nquery, {page: ePage})
      const summary = searchSummary(q, lat, lon, d)
      const pageObject = {
        type: 'CollectionPage',
        id: searchID(pa),
        summary: `page ${ePage} of ${summary}`,
        items: items.slice(offset, offset + limit)
      }

      if (items.length > offset + limit - 1) {
        const npa = _.extend({}, nquery, {page: ePage + 1})
        pageObject.next = searchID(npa)
      }

      if (ePage > 1) {
        const ppa = _.extend({}, nquery, {page: ePage - 1})
        pageObject.prev = searchID(ppa)
      }

      const qa = _.omit(nquery, 'page')

      const collection = {
        type: 'Collection',
        summary: summary,
        id: searchID(qa),
        totalItems: items.length
      }

      const doc = (page)
        ? _.extend({partOf: collection}, pageObject)
        : _.extend({first: _.extend({partOf: collection.id}, pageObject)}, collection)

      debug(`Returning AS2 for ${nquery}`)

      as2Document(req, res, doc)
    } catch (err) {
      return next(err)
    }
  })

  osm.param('id', (req, res, next, id) => {
    if (!id.match(/^\d+$/)) {
      res.sendStatus(404, 'Not found')
    } else {
      next()
    }
  })

  // Node route

  osm.get('/node/:id', async (req, res, next) => {
    debug('osm /node/:id route matched')
    const {id} = req.params
    debug(`id = ${id}`)
    const query = `node(${id})`
    try {
      const njson = await interpret(req.app, query)
      if (!_.isObject(njson) || !_.isArray(njson.elements)) {
        throw new Error(`Unexpected result type: ${typeof njson}`)
      }
      if (njson.elements.length !== 1) {
        throw new Error(`Unexpected result length: ${njson.length}`)
      }
      const [nplace] = njson.elements
      debug(`Converting overpass data to AS2 for ${id}`)
      const as2place = nodeToAS2(nplace)
      debug(as2place)
      debug(`Returning AS2 for ${id}`)
      as2Document(req, res, as2place)
    } catch (err) {
      debug(`ERROR: '${err.message}' for ${id}`)
      next(err)
    }
  })

  // Way route

  osm.get('/way/:id', async (req, res, next) => {
    debug('osm /way/:id route matched')
    const {id} = req.params
    debug(`id = ${id}`)
    const query = `way(${id})`
    try {
      const njson = await interpret(req.app, query)
      if (!_.isObject(njson) || !_.isArray(njson.elements)) {
        throw new Error(`Unexpected result type: ${typeof njson}`)
      }
      if (njson.elements.length !== 1) {
        throw new Error(`Unexpected result length: ${njson.length}`)
      }
      const [nplace] = njson.elements
      debug(`Converting overpass data to AS2 for ${id}`)
      const as2place = wayToAS2(nplace)
      debug(as2place)
      debug(`Returning AS2 for ${id}`)
      as2Document(req, res, as2place)
    } catch (err) {
      debug(`ERROR: '${err.message}' for ${id}`)
      next(err)
    }
  })

  return osm
}

module.exports = makeOSM
