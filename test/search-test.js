// search-test.js -- test script for the search endpoint
//
//  Copyright 2017 Evan Prodromou <evan@prodromou.name>
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

const querystring = require('querystring')
const fetch = require('node-fetch')
const _ = require('lodash')
const debug = require('debug')('places-pub:search-test')

const vows = require('perjury')
const {assert} = vows

const Server = require('./server')
const env = require('./env')
const validate = require('./validate')

const ACCEPT = 'application/activity+json;1.0,application/ld+json;0.5,application/json;0.1'
const HEADERS = {
  'Accept': ACCEPT
}
const URL_ROOT = `http://${env.PLACES_PUB_HOSTNAME}:${env.PLACES_PUB_PORT}`

const searchBatch = (params, tests) => {
  const batch = {
    topic: async function () {
      const qp = querystring.stringify(params)
      const url = `${URL_ROOT}/osm/search?${qp}`
      return fetch(url, {headers: HEADERS})
    },
    'it works': (err, res) => {
      assert.ifError(err)
      assert.isObject(res)
      assert.equal(res.status, 200)
    },
    'and we examine the body of the response': {
      topic (res) {
        return res.json()
      },
      'it looks like the search results we expect': (err, results) => {
        assert.ifError(err)
        debug(results)
        validate.document(results)
        validate.searchResults(results)
      }
    }
  }
  if (tests) {
    _.extend(batch['and we examine the body of the response'], tests)
  }
  return batch
}

const searchPageBatch = (params, tests) => {
  const batch = {
    topic: async function () {
      const qp = querystring.stringify(params)
      const url = `${URL_ROOT}/osm/search?${qp}`
      return fetch(url, {headers: HEADERS})
    },
    'it works': (err, res) => {
      assert.ifError(err)
      assert.isObject(res)
      assert.equal(res.status, 200)
    },
    'and we examine the body of the response': {
      topic (res) {
        return res.json()
      },
      'it looks like the search results we expect': (err, results) => {
        assert.ifError(err)
        debug(results)
        validate.document(results)
        validate.page(results)
      }
    }
  }
  if (tests) {
    _.extend(batch['and we examine the body of the response'], tests)
  }
  return batch
}

const SCHWARTZS = "Schwartz's"
const LACOSTANERA_NAME = 'La Costanera'
const LACOSTANERA_LATLON = {
  lat: 37.54615,
  lon: -122.51410
}
const LACOSTANERA_NAME_LATLON = _.extend({q: LACOSTANERA_NAME}, LACOSTANERA_LATLON)
const LACOSTANERA_NAME_LATLON_D = _.extend({d: 250}, LACOSTANERA_NAME_LATLON)
const LACOSTANERA_LATLON_D = _.extend({d: 250}, LACOSTANERA_LATLON)
const THE_MAYFLOWER = {
  lat: 51.50179,
  lon: -0.05358,
  q: 'The Mayflower'
}
const LA_BANQUISE = {
  q: 'La Banquise'
}
const CASGRAIN = {
  lat: 45.52609,
  lon: -73.59526
}
const CASGRAIN_D = _.extend({d: 250}, CASGRAIN)
const CASGRAIN_D_PAGE = _.extend({page: 1}, CASGRAIN_D)

vows.describe('search endpoint')
  .addBatch({
    'When we start the app': {
      topic: async function () {
        const server = new Server(env)
        await server.start()
        return server
      },
      'it works': (err, server) => {
        assert.ifError(err)
        assert.isObject(server)
      },
      'and we search for a node by name': searchBatch({q: SCHWARTZS}),
      'and we search for a node by lat/lon': searchBatch(LACOSTANERA_LATLON, {
        'it includes our expected result': (err, results) => {
          assert.ifError(err)
          assert.isObject(results.first)
          assert.isArray(results.first.items)
          assert.greater(results.first.items.length, 0)
          assert.ok(_.some(results.first.items, (item) => {
            return (item.name === LACOSTANERA_NAME)
          }))
        }
      }),
      'and we search for a node by name and lat/lon': searchBatch(LACOSTANERA_NAME_LATLON, {
        'it includes our expected result': (err, results) => {
          assert.ifError(err)
          assert.isArray(results.first.items)
          assert.greater(results.first.items.length, 0)
          assert.ok(_.some(results.first.items, (item) => {
            return (item.name === LACOSTANERA_NAME)
          }))
        }
      }),
      'and we search for a node by name, lat/lon and distance': searchBatch(LACOSTANERA_NAME_LATLON_D),
      'and we search for a node by lat/lon and distance': searchBatch(LACOSTANERA_LATLON_D),
      'and we search for a way by name and lat/lon': searchBatch(THE_MAYFLOWER, {
        'it includes our expected result': (err, results) => {
          assert.ifError(err)
          assert.isArray(results.first.items)
          assert.greater(results.first.items.length, 0)
          assert.ok(_.some(results.first.items, (item) => {
            return item.id.match('way/444744679')
          }))
        }
      }),
      'and we search for mixed ways and nodes by name': searchBatch(LA_BANQUISE, {
        'it includes our expected result': (err, results) => {
          assert.ifError(err)
          assert.isArray(results.first.items)
          assert.greater(results.first.items.length, 0)
          assert.ok(_.some(results.first.items, (item) => {
            return item.id.match('way/356350765')
          }))
        }
      }),
      'and we search for mixed ways and nodes by lat/lon': searchBatch(CASGRAIN, {
        'it includes our expected result': (err, results) => {
          assert.ifError(err)
          assert.isArray(results.first.items)
          assert.greater(results.first.items.length, 0)
          assert.ok(_.some(results.first.items, (item) => {
            return item.id.match('way/172575402')
          }))
        }
      }),
      'and we search for mixed ways and nodes by lat/lon and distance': searchBatch(CASGRAIN_D),
      'and we search for a page of mixed ways and nodes by lat/lon and distance': searchPageBatch(CASGRAIN_D_PAGE),
      'teardown': (server) => {
        return server.stop()
      }
    }
  })
  .export(module)
