// app-test.js -- main entry point for places.pub server
//
//  Copyright 2017 Evan Prodromou <evan@prodromou.name>
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

const fetch = require('node-fetch')
const _ = require('lodash')

const vows = require('perjury')
const {assert} = vows

const Server = require('./server')
const env = require('./env')
const validate = require('./validate')

const ACCEPT = 'application/activity+json;1.0,application/ld+json;0.5,application/json;0.1'
const HEADERS = {
  'Accept': ACCEPT
}

vows.describe('node endpoint')
  .addBatch({
    'When we start the app': {
      topic: async function () {
        const server = new Server(env)
        await server.start()
        return server
      },
      'it works': (err, server) => {
        assert.ifError(err)
        assert.isObject(server)
      },
      'and we fetch a node by ID': {
        topic () {
          const url = `http://${env.PLACES_PUB_HOSTNAME}:${env.PLACES_PUB_PORT}/osm/node/343490371`
          return fetch(url, {headers: HEADERS})
        },
        'it works': (err, res) => {
          assert.ifError(err)
          assert.equal(res.status, 200)
        },
        'the headers look correct': (err, res) => {
          assert.ifError(err)
          assert.ok(res.headers.has('ETag'), 'response does not have an ETag')
          assert.ok(res.headers.has('Last-Modified'), 'response does not have a Last-Modified')
        },
        'and we examine the body': {
          topic (res) {
            return res.json()
          },
          'it looks correct': (err, body) => {
            assert.ifError(err)
            const expected = {
              id: '343490371',
              type: 'node',
              name: "Schwartz's",
              'street-address': '3895 Boulevard Saint-Laurent',
              'postal-code': 'H2W 1X9'
            }
            validate.document(body)
            validate.place(body, expected)
          }
        },
        'and we fetch the same URL again': {
          topic (res) {
            const url = `http://${env.PLACES_PUB_HOSTNAME}:${env.PLACES_PUB_PORT}/osm/node/343490371`
            const headers = _.extend({
              'If-None-Match': res.headers.get('ETag'),
              'If-Modified-Since': res.headers.get('Last-Modified')
            }, HEADERS)
            return fetch(url, {headers: headers})
          },
          'it gives a Not Modified response': (err, res) => {
            assert.ifError(err)
            assert.equal(res.status, 304)
          }
        }
      },
      'and we fetch a node that does not exist': {
        topic: async function () {
          const url = `http://${env.PLACES_PUB_HOSTNAME}:${env.PLACES_PUB_PORT}/osm/node/WNOTANID`
          return fetch(url, {headers: HEADERS})
        },
        'it works': (err, res) => {
          assert.ifError(err)
          assert.isObject(res)
          assert.equal(res.status, 404)
        }
      },
      'teardown': (server) => {
        return server.stop()
      }
    }
  })
  .export(module)
