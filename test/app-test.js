// app-test.js -- main entry point for places.pub server
//
//  Copyright 2017 Evan Prodromou <evan@prodromou.name>
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

const fs = require('fs')
const path = require('path')

const fetch = require('node-fetch')

const vows = require('perjury')
const {assert} = vows
const _ = require('lodash')
const pkg = path.join(__dirname, '..', 'package.json')
const {name, version} = JSON.parse(fs.readFileSync(pkg, {encoding: 'utf8'}))
const serverTag = `${name}/${version}`

const Server = require('./server')
const env = require('./env')

const checkUrl = (rel, tests = {}) => {
  assert.equal(rel[0], '/')
  const batch = {
    topic: async function () {
      return fetch(`http://${env.PLACES_PUB_HOSTNAME}:${env.PLACES_PUB_PORT}${rel}`)
    },
    'it works': (err, res) => {
      assert.ifError(err)
      assert.isObject(res)
      assert.equal(res.status, 200)
      assert.equal(res.headers.get('Server'), serverTag)
    },
    'and we examine the body': {
      async topic (res) {
        return res.text()
      },
      'it works': (err, body) => {
        assert.ifError(err)
      }
    }
  }
  _.extend(batch['and we examine the body'], tests)
  return batch
}

vows.describe('app loads and listens on correct port')
  .addBatch({
    'When we start the app': {
      topic: async function () {
        const server = new Server(env)
        await server.start()
        return server
      },
      'it works': (err, server) => {
        assert.ifError(err)
        assert.isObject(server)
      },
      'and we fetch the home page': checkUrl('/'),
      'and we fetch the api page': checkUrl('/api'),
      'and we fetch the Bootstrap CSS': checkUrl('/bootstrap/css/bootstrap.min.css'),
      'and we fetch the JQuery JS': checkUrl('/jquery/jquery.min.js'),
      'and we fetch the Bootstrap JS': checkUrl('/bootstrap/js/bootstrap.min.js'),
      'teardown': (server) => {
        return server.stop()
      }
    }
  })
  .export(module)
