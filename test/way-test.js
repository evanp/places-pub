// app-test.js -- main entry point for places.pub server
//
//  Copyright 2017 Evan Prodromou <evan@prodromou.name>
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

const fetch = require('node-fetch')
const _ = require('lodash')

const vows = require('perjury')
const {assert} = vows

const Server = require('./server')
const env = require('./env')
const validate = require('./validate')

const ACCEPT = 'application/activity+json;1.0,application/ld+json;0.5,application/json;0.1'
const HEADERS = {
  'Accept': ACCEPT
}

vows.describe('way endpoint')
  .addBatch({
    'When we start the app': {
      topic: async function () {
        const server = new Server(env)
        await server.start()
        return server
      },
      'it works': (err, server) => {
        assert.ifError(err)
        assert.isObject(server)
      },
      'and we fetch a way by ID': {
        topic () {
          const url = `http://${env.PLACES_PUB_HOSTNAME}:${env.PLACES_PUB_PORT}/osm/way/356350765`
          return fetch(url, {headers: HEADERS})
        },
        'it works': (err, res) => {
          assert.ifError(err)
          assert.equal(res.status, 200)
        },
        'the headers look correct': (err, res) => {
          assert.ifError(err)
          assert.ok(res.headers.has('ETag'), 'response does not have an ETag')
          assert.ok(res.headers.has('Last-Modified'), 'response does not have a Last-Modified')
        },
        'and we examine the body': {
          topic (res) {
            return res.json()
          },
          'it works': (err, body) => {
            assert.ifError(err)
            const expected = {
              id: '356350765',
              type: 'way',
              name: 'La Banquise',
              latitude: 45.5252489,
              longitude: -73.5746408
            }
            validate.document(body)
            validate.place(body, expected)
          }
        },
        'and we fetch the same URL again': {
          topic (res) {
            const url = `http://${env.PLACES_PUB_HOSTNAME}:${env.PLACES_PUB_PORT}/osm/way/356350765`
            const headers = _.extend({
              'If-None-Match': res.headers.get('ETag'),
              'If-Modified-Since': res.headers.get('Last-Modified')
            }, HEADERS)
            return fetch(url, {headers: headers})
          },
          'it gives a Not Modified response': (err, res) => {
            assert.ifError(err)
            assert.equal(res.status, 304)
          }
        }
      },
      'and we fetch another way by ID': {
        topic: async function () {
          const url = `http://${env.PLACES_PUB_HOSTNAME}:${env.PLACES_PUB_PORT}/osm/way/444744679`
          const res = await fetch(url, {headers: HEADERS})
          if (res.status !== 200) {
            const body = await res.text()
            throw new Error(`Error ${res.status}: ${body}`)
          }
          return res.json()
        },
        'it works': (err, body) => {
          assert.ifError(err)
          const expected = {
            id: '444744679',
            type: 'way',
            name: 'The Mayflower',
            'street-address': '117 Rotherhithe Street',
            'postal-code': 'SE16 4NF',
            'locality': 'London',
            'country': 'GB'
          }
          validate.document(body)
          validate.place(body, expected)
        }
      },
      'and we fetch a way that does not exist': {
        topic: async function () {
          const url = `http://${env.PLACES_PUB_HOSTNAME}:${env.PLACES_PUB_PORT}/osm/way/NOTANID`
          return fetch(url, {headers: HEADERS})
        },
        'it works': (err, res) => {
          assert.ifError(err)
          assert.isObject(res)
          assert.equal(res.status, 404)
        }
      },
      'teardown': (server) => {
        return server.stop()
      }
    }
  })
  .export(module)
