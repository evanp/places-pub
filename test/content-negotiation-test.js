// app-test.js -- main entry point for places.pub server
//
//  Copyright 2017 Evan Prodromou <evan@prodromou.name>
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

const qs = require('querystring')
const fetch = require('node-fetch')
const vows = require('perjury')
const {assert} = vows

const Server = require('./server')
const env = require('./env')
const validate = require('./validate')

const AS2_TYPE = 'application/activity+json'
const JSONLD_TYPE = 'application/ld+json'
const JSON_TYPE = 'application/json'

const URL_ROOT = `http://${env.PLACES_PUB_HOSTNAME}:${env.PLACES_PUB_PORT}`

const fetchFeature = (id, ftype, type) => {
  return {
    topic () {
      const url = `${URL_ROOT}/osm/${ftype}/${id}`
      const headers = {
        'Accept': type
      }
      return fetch(url, {headers: headers})
    },
    'it works': (err, res) => {
      assert.ifError(err)
      assert.equal(res.status, 200)
    },
    'the headers look correct': (err, res) => {
      assert.ifError(err)
      assert.ok(res.headers.has('Content-Type'), 'response does not have a content type')
      const contentType = res.headers.get('Content-Type')
      const baseType = (contentType.indexOf(';') === -1) ? contentType : contentType.substr(0, contentType.indexOf(';'))
      assert.equal(baseType, type)
    },
    'and we examine the body': {
      topic (res) {
        return res.json()
      },
      'it looks correct': (err, body) => {
        assert.ifError(err)
        validate.document(body)
        validate.place(body)
      }
    }
  }
}

const fetchNode = (id, type) => {
  return fetchFeature(id, 'node', type)
}

const fetchWay = (id, type) => {
  return fetchFeature(id, 'way', type)
}

const search = (q, lat, lon, type) => {
  return {
    topic () {
      const params = qs.stringify({q: q, lat: lat, lon: lon})
      const url = `${URL_ROOT}/osm/search?${params}`
      const headers = {
        'Accept': type
      }
      return fetch(url, {headers: headers})
    },
    'it works': (err, res) => {
      assert.ifError(err)
      assert.equal(res.status, 200)
    },
    'the headers look correct': (err, res) => {
      assert.ifError(err)
      assert.ok(res.headers.has('Content-Type'), 'response does not have a content type')
      const contentType = res.headers.get('Content-Type')
      const baseType = (contentType.indexOf(';') === -1) ? contentType : contentType.substr(0, contentType.indexOf(';'))
      assert.equal(baseType, type)
    },
    'and we examine the body': {
      topic (res) {
        return res.json()
      },
      'it looks correct': (err, body) => {
        assert.ifError(err)
        validate.document(body)
        validate.searchResults(body)
      }
    }
  }
}

const searchPage = (q, lat, lon, page, type) => {
  return {
    topic () {
      const params = qs.stringify({q: q, lat: lat, lon: lon, page: page})
      const url = `${URL_ROOT}/osm/search?${params}`
      const headers = {
        'Accept': type
      }
      return fetch(url, {headers: headers})
    },
    'it works': (err, res) => {
      assert.ifError(err)
      assert.equal(res.status, 200)
    },
    'the headers look correct': (err, res) => {
      assert.ifError(err)
      assert.ok(res.headers.has('Content-Type'), 'response does not have a content type')
      const contentType = res.headers.get('Content-Type')
      const baseType = (contentType.indexOf(';') === -1) ? contentType : contentType.substr(0, contentType.indexOf(';'))
      assert.equal(baseType, type)
    },
    'and we examine the body': {
      topic (res) {
        return res.json()
      },
      'it looks correct': (err, body) => {
        assert.ifError(err)
        validate.document(body)
        validate.page(body)
      }
    }
  }
}

vows.describe('content negotiation')
  .addBatch({
    'When we start the app': {
      topic: async function () {
        const server = new Server(env)
        await server.start()
        return server
      },
      'it works': (err, server) => {
        assert.ifError(err)
        assert.isObject(server)
      },
      'and we fetch a node accepting only ActivityStreams 2.0': fetchNode(343490371, AS2_TYPE),
      'and we fetch a node accepting only JSON-LD': fetchNode(343490371, JSONLD_TYPE),
      'and we fetch a node accepting only JSON': fetchNode(343490371, JSON_TYPE),
      'and we fetch a way accepting only ActivityStreams 2.0': fetchWay(356350765, AS2_TYPE),
      'and we fetch a way accepting only JSON-LD': fetchWay(356350765, JSONLD_TYPE),
      'and we fetch a way accepting only JSON': fetchWay(356350765, JSON_TYPE),
      'and we search accepting only ActivityStreams 2.0': search('Denali', 63.06912, -151.00624, AS2_TYPE),
      'and we search accepting only JSON-LD': search('Denali', 63.06912, -151.00624, JSONLD_TYPE),
      'and we search accepting only JSON': search('Denali', 63.06912, -151.00624, JSON_TYPE),
      'and we search with a page accepting only ActivityStreams 2.0': searchPage('Denali', 63.06912, -151.00624, 1, AS2_TYPE),
      'and we search with a page accepting only JSON-LD': searchPage('Denali', 63.06912, -151.00624, 1, JSONLD_TYPE),
      'and we search with a page accepting only JSON': searchPage('Denali', 63.06912, -151.00624, 1, JSON_TYPE),
      'teardown': (server) => {
        return server.stop()
      }
    }
  })
  .export(module)
