// validate.js -- validate results
//
//  Copyright 2017 Evan Prodromou <evan@prodromou.name>
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

const {assert} = require('perjury')
const _ = require('lodash')
const debug = require('debug')('places-pub:validate')

const env = require('./env')

const AS2 = 'https://www.w3.org/ns/activitystreams'
const VCARD = 'http://www.w3.org/2006/vcard/ns#'
const CC = 'http://creativecommons.org/ns#'
const ODBL = 'http://opendatacommons.org/licenses/odbl/'

const document = (body) => {
  assert.isArray(body['@context'], '@context is not an array')
  assert.lengthOf(body['@context'], 2, '@context is not a 2-element array')
  assert.isString(body['@context'][0], '@context[0] is not a string')
  assert.equal(body['@context'][0], AS2, '@context[0] is not AS2 context')
  assert.isObject(body['@context'][1], 'Context element 1 is not an object')
  assert.deepEqual(body['@context'][1], {vcard: VCARD, cc: CC}, '@context[1] is not vcard + cc')
  assert.equal(body['cc:license'], ODBL, 'License value not set')
  assert.equal(body['cc:attributionName'], 'OpenStreetMap contributors', 'Attribution name not set')
  assert.equal(body['cc:attributionURL'], 'http://www.openstreetmap.org/copyright', 'Attribution url not set')
}

const place = (body, expected = {}) => {
  assert.isObject(body, 'Body of place is not an object')
  assert.isString(body.type, 'Type of place is not a string')
  assert.equal(body.type, 'Place')
  assert.isString(body.id, 'ID of place is not a string')
  if (expected.id && expected.type) {
    assert.equal(body.id, `${env.PLACES_PUB_ROOT}/osm/${expected.type}/${expected.id}`)
  }
  assert.isString(body.name, 'Name of place is not a string')
  assert.greater(body.name.length, 0, 'Name of place is zero-length')
  if (expected.name) {
    assert.equal(body.name, expected.name)
  }
  assert.isNumber(body.latitude, 'Latitude of place is not a number')
  if (expected.latitude) {
    assert.inDelta(body.latitude, expected.latitude, 0.1)
  }
  assert.isNumber(body.longitude, 'Longitude of place is not a number')
  if (expected.longitude) {
    assert.inDelta(body.longitude, expected.longitude, 0.1)
  }
  const props = ['street-address', 'locality', 'region', 'country-name', 'postal-code']
  if (_.some(props, (prop) => _.has(expected, prop))) {
    assert.include(body, 'vcard:hasAddress')
  }
  if (_.has(body, 'vcard:hasAddress')) {
    assert.isObject(body['vcard:hasAddress'], 'Address of Place is not an object')
    const addr = body['vcard:hasAddress']
    assert.equal(addr.type, 'vcard:Address', 'vcard address object has wrong type')
    const props = ['street-address', 'locality', 'region', 'country-name', 'postal-code']
    let prop = null
    for (prop of props) {
      debug(`testing ${prop}`)
      if (expected[prop]) {
        assert.equal(addr[`vcard:${prop}`], expected[prop])
      }
    }
  }
  assert.includes(body, 'updated')
}

const page = (page, expected = []) => {
  assert.equal(page.type, 'CollectionPage', 'page has wrong type')
  assert.isString(page.id, 'page has no id')
  assert.includes(page, 'partOf')
  assert.isString(page.summary, 'page has no summary')
  assert.isArray(page.items, 'page has no items')
  for (const i in page.items) {
    const item = page.items[i]
    if (expected.length > i) {
      place(item, expected[i], false)
    } else {
      place(item, {}, false)
    }
  }
}

const searchResults = (body, expected = []) => {
  assert.isObject(body, 'search results is not an object')
  assert.equal(body.type, 'Collection')
  assert.isString(body.id, 'Search results have no ID')
  assert.isString(body.summary, 'Search results have no summary')
  assert.isNumber(body.totalItems, 'Search results have no totalItems')
  assert.greater(body.totalItems, 0, 'Search results totalItems <= 0')
  assert.isObject(body.first, 'Search results has no first page')
  assert.isArray(body.first.items, 'Search results page has no items')
  assert.ok(body.totalItems >= body.first.items.length, 'page items array longer than totalItems')
  assert.equal(body.first.partOf, body.id, 'page.partOf does not match collection ID')
  page(body.first, expected)
}

module.exports = {
  place: place,
  searchResults: searchResults,
  document: document,
  page: page
}
