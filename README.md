# places.pub

[![pipeline status](https://gitlab.com/evanp/places-pub/badges/master/pipeline.svg)](https://gitlab.com/evanp/places-pub/commits/master)

This is the source code for the [places.pub](https://places.pub/) service. It
provides a home for [Activity Streams 2.0](https://www.w3.org/TR/activitystreams-core/) [Place](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-place) objects,
using the [OpenStreetMap](https://openstreetmap.org/) database.

## LICENSE

Copyright 2017 Evan Prodromou <mailto:evan@prodromou.name>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## How to use it

The [API](https://places.pub/api) page documents the API for the service.
