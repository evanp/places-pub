FROM node:8-alpine

WORKDIR /opt/places-pub

COPY package.json .
COPY package-lock.json .

RUN npm install

COPY . .

EXPOSE 80

CMD ["npm", "start"]
